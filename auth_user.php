<?php
    session_start();

    $hostname = $_SERVER['HTTP_HOST'];
    $path = dirname($_SERVER['PHP_SELF']);


    if (!isset($_SESSION['logedInUser']) || !$_SESSION['logedInUser']) {
        if (empty($_SERVER['HTTPS']))
            header('Location: http://'.$hostname.($path == '/' ? '' : $path).'/login.php');
        else
            header('Location: https://'.$hostname.($path == '/' ? '' : $path).'/login.php');
        exit;
     }
?>