<?php include('auth.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>Details User</title>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>
</head>
<body>
<?php include('db_connection.php'); ?>
<?php include('menu.php'); ?>

<div class='container'>
<br/>

<div id="responseContainer" class="alert hidden" role="alert">
    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
    <span class="sr-only">Error:</span>
    <span id="response"></span>
</div>

<h2>User</h2>


<form id='edit_user' action='' method='post'>
    <table id='user' class='table'>
        <tbody>
        <tr>
            <th>user_idnr</th>
            <td id='user_idnr'></td>
            <td></td>
        </tr>
        <tr>
            <th>User ID</th>
            <td><input id='inputUserId' name='userid' type='text' size='30'></td>
            <td><i>The ID/Login, e.g. user@domain.com</i></td>
        </tr>
        <tr>
            <th>Password</th>
            <td><input id='password' name='passwd' type='text' value='' size='30'> Type:
                <select id='hash_algorithm' name='encryption_type'>
                    <option name='blank'></option>
                    <option name='plaintext'>plaintext</option>
                    <option name='md5-hash'>md5-hash</option>
                    <option name='md5-digest'>md5-digest</option>
                    <option name='whirlpool'>whirlpool</option>
                    <option name='sha512'>sha512</option>
                    <option name='sha256'>sha256</option>
                    <option name='sha1'>sha1</option>
                    <option name='tiger'>tiger</option>
                </select>
                <br><a class="btn btn-default" href='javascript:generatePassword()'>generate a Password</a></td>
            <td><i>If you need a plaintext password use the blank option, 'plaintext' will not work.</i></td>
        </tr>
        <tr>
            <th>Hashed Password</th>
            <td><textarea id='hashed_password' name='hashed_password' rows='4' cols='30'></textarea></td>
            <td><i>This will be stored in the database.</i></td>
        </tr>
        <tr>
            <th>Mailbox size</th>
            <td><span id="mbox_cur_mb"></span><span> MB / </span><input id='maxmail_size' name='maxmail_size'
                                                                        type='text' size='10'><span> MB</span></td>
            <td><i>0 means unlimited space.</i></td>
        </tr>
        <tr>
            <th>Last Login</th>
            <td id="last_login"></td>
            <td></td>
        </tr>
        </tbody>
</table>

    <a class="btn btn-default" href="javascript:saveUser()">Save</a>
    <a class="btn btn-default" href="javascript:loadUser()">Reset Changes</a>
    <a class="btn btn-default" href='javascript:delUser(<?php echo $_GET["user_idnr"] ?>)'>Delete User</a>

</form>

<br>
<hr align="left"></hr>

<!-- DBMail aliases grouped by deliver_to -->

<h2>Aliases</h2>

<div id='aliases'>
    <table>
        <tbody></tbody>
    </table>
    <div>Number of aliases: <span id="numberOfAliases"></span></div>
</div>

<div id='new_alias_container'>
    <form id='new_alias' action='JavaScript:saveAlias()' method='post'>
        <input type='text' name='alias' id='inputAlias' size='30'><a class="btn btn-default"
                                                                     href='JavaScript:saveAlias()'>Add</a>
    </form>
</div>


<script type="text/javascript">
    function loadAliases() {
        $("#aliases tbody > tr").remove();
        $.ajax({
            dataType: "json",
            type: "GET",
            url: "api.php",
            data: {
                get: "forwards",
                user_idnr: <?php echo $_GET['user_idnr']; ?>
            },
            context: document.body
        }).done(function (response) {
            if (response.status == 'OK') {
                var aliases = response.result[0].aliases;
                $("#numberOfAliases").text(' ' + aliases.length);

                $.each(aliases, function (index_aliases, alias) {
                    var row = "<tr>";
                    row += "<td>";
                    row += "<a class='forward_del' href='JavaScript: delAlias(" + escapeHTML(alias.id) + ")'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></a> ";
                    row += escapeHTML(alias.alias);
                    row += "</td></tr>";
                    $("#aliases tbody").append(row);
                });
            } else {
                $("#responseContainer").addClass("alert-danger");
                $("#responseContainer").removeClass("hidden");
                $("#response").text(response.result);
            }
        });
    }
    function escapeHTML(str) {
        return str.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    }
</script>




<!-- Keylistener 'press the enter-key to create a new alias, if the form is fucused'. -->
<script type="text/javascript">
    function keydown_alias(event) {
        if (!event)
            event = window.event;
        if (event.which) {
            keycode = event.which;
        } else if (event.keyCode) {
            keycode = event.keyCode;
        }
        if (keycode == 13) {
            document.forms['new_alias'].submit();
        }
    }
    document.forms['new_alias'].onkeydown = keydown_alias;
</script>


<hr align="left"></hr>

<!-- DBMail sievescripts for that user -->

<h2>Sievescripts</h2>

<div id='sievescripts'>
</div>


<script type="text/javascript">
    function loadSievescripts() {
        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        }
        else {// code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("sievescripts").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "list_sievescripts.php?user_idnr=" + document.getElementById('user_idnr').innerHTML, true);
        xmlhttp.send();
    }
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<script type="text/javascript">
    // Keylistener 'Update the hash password value
    function updatePasswordHash(){
        if (($("#hash_algorithm").val() != "") && ($("#hash_algorithm").val() != "plaintext")){
            $.ajax({
                url: "hash_password.php",
                data: { text: $("#password").val(), algorithm:  $("#hash_algorithm").val() },
                context: document.body
            }).done(function(hash) {
                $( "#hashed_password" ).val(hash);
            });
        } else {
            $( "#hashed_password" ).val($("#password").val());
        }
    }
    $( "#password" ).keyup(function() {
        updatePasswordHash();
    });
    $( "#hash_algorithm" ).change(function() {
        updatePasswordHash();
    });

    function generatePassword(){
        $.ajax({
            url: "generate_password.php",
            context: document.body
        }).done(function(hash) {
            $( "#password" ).val(hash);
            updatePasswordHash();
        });

    }
    // Save new alias
    function saveAlias() {
        $.ajax({
            dataType: "json",
            type: "POST",
            url: "api.php",
            data: {
                post: "forward",
                alias: $("#inputAlias").val(),
                deliver_to: <?php echo $_GET['user_idnr']; ?>
            },
            context: document.body
        }).done(function (response) {
            $("#responseContainer").removeClass("hidden");
            $("#response").text(response.result);

            if (response.status == 'OK') {
                $("#responseContainer").removeClass("alert-danger");
                $("#responseContainer").addClass("alert-success");
            } else {
                $("#responseContainer").removeClass("alert-success");
                $("#responseContainer").addClass("alert-danger");
            }
            loadAliases();
        });
    }
    function delAlias(alias_idnr) {
        Check = confirm("Delete that forward? Alias=" + alias_idnr);
        if (Check == true) {
            $.ajax({
                dataType: "json",
                type: "GET",
                url: "api.php",
                data: {
                    delete: "alias",
                    id: alias_idnr
                },
                context: document.body
            }).done(function (response) {
                $("#responseContainer").removeClass("hidden");
                $("#response").text(response.result);

                if (response.status == 'OK') {
                    $("#responseContainer").removeClass("alert-danger");
                    $("#responseContainer").addClass("alert-success");
                } else {
                    $("#responseContainer").removeClass("alert-success");
                    $("#responseContainer").addClass("alert-danger");
                }
                loadAliases();
            });
        }
    }
    function loadUser() {
        $.ajax({
            dataType: "json",
            type: "GET",
            url: "api.php",
            data: {
                get: "user",
                user_idnr: <?php echo $_GET['user_idnr']; ?>
            },
            context: document.body
        }).done(function (response) {
            if (response.status == 'OK') {
                var user = response.result;
                $("#user_idnr").text(user.user_idnr);
                $("#inputUserId").val(user.userid);
                $("#hash_algorithm").val(user.encryption_type);
                $("#hashed_password").val(user.passwd);
                $("#mbox_cur_mb").text(user.curmail_size);
                $("#maxmail_size").val(user.maxmail_size)
                if (user.last_login == '1979-11-03 22:05:58') {
                    $("#last_login").text("never");
                } else {
                    $("#last_login").text(user.last_login);
                }


                //$("#average_messages_day").text(result.average_messages_day);


            } else {
                $("#responseContainer").addClass("alert-danger");
                $("#responseContainer").removeClass("hidden");
                $("#response").text(response.result);
            }
        });
        last_login
    }

    <!-- AJAX-Script for saving an user -->
    function saveUser() {
        $.ajax({
            dataType: "json",
            type: "POST",
            url: "api.php",
            data: {
                post: "user",
                user_idnr: $("#user_idnr").text(),
                userid: $("#inputUserId").val(),
                passwd: $("#hashed_password").val(),
                encryption_type: $("#hash_algorithm").val(),
                maxmail_size: $("#maxmail_size").val()
            },
            context: document.body
        }).done(function (response) {
            $("#responseContainer").removeClass("hidden");
            $("#response").text(response.result);

            if (response.status == 'OK') {
                $("#responseContainer").removeClass("alert-danger");
                $("#responseContainer").addClass("alert-success");
            } else {
                $("#responseContainer").removeClass("alert-success");
                $("#responseContainer").addClass("alert-danger");
            }
        });
    }
    function delUser(user_idnr) {
        Check = confirm("Delete that user? user_idnr=" + user_idnr);
        if (Check == true) {
            $.ajax({
                dataType: "json",
                type: "GET",
                url: "api.php",
                data: {
                    delete: "user",
                    user_idnr: user_idnr
                },
                context: document.body
            }).done(function (response) {
                $("#responseContainer").removeClass("hidden");
                $("#response").text(response.result);

                if (response.status == 'OK') {
                    $("#responseContainer").removeClass("alert-danger");
                    $("#responseContainer").addClass("alert-success");
                    $(".container :input").attr("disabled", true);
                    $(".container a").attr("disabled", true);

                } else {
                    $("#responseContainer").removeClass("alert-success");
                    $("#responseContainer").addClass("alert-danger");
                }
            });
        }
    }
    jQuery(window).load(function () {
        $("#menu_users").addClass("active");
        loadUser()
        loadAliases();
    });
</script>

<div id='response'></div>
</div>
</body>

<script src="js/bootstrap.min.js"></script>
</html>
 