<ul class="nav nav-tabs">
    <li id="menu_home" role="presentation"><a href="index.php"><span class="glyphicon glyphicon-home"
                                                                     aria-hidden="true"></span> Home</a></li>
    <li id="menu_users" role="presentation"><a href="users.php"><span class="glyphicon glyphicon-user"
                                                                      aria-hidden="true"></span> Users</a></li>
    <li id="menu_forwards" role="presentation"><a href="forwards.php"><span class="glyphicon glyphicon-arrow-right"
                                                                            aria-hidden="true"></span> Forwards</a></li>
    <li id="menu_logout" role="presentation"><a href="logout.php"><span class="glyphicon glyphicon-log-out"
                                                                        aria-hidden="true"></span> Logout</a></li>
</ul>