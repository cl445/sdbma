<?php
require_once('config.php');

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    session_start();
    $username = $_POST['username'];
    $password = $_POST['password'];

    $hostname = $_SERVER['HTTP_HOST'];
    $path = dirname($_SERVER['PHP_SELF']);

    // check login and password
    if ($username == SDBMA_LOGIN && $password == SDBMA_PASSWORD) {
        $loginfailed = false;
        $_SESSION['logedInAdmin'] = true;

        // forward to startpage
        if ($_SERVER['SERVER_PROTOCOL'] == 'HTTP/1.1') {
            if (php_sapi_name() == 'cgi') {
                header('Status: 303 See Other');
            } else {
                header('HTTP/1.1 303 See Other');
            }
        }
        if (empty($_SERVER['HTTPS']))
            header('Location: http://' . $hostname . ($path == '/' ? '' : $path) . '/index.php');
        else
            header('Location: https://' . $hostname . ($path == '/' ? '' : $path) . '/index.php');
        exit;
    } elseif ($username != '') {
        include('db_connection.php');
        $STH = $DBH->prepare('SELECT userid, passwd, encryption_type FROM dbmail_users WHERE userid=:userid');
        $STH->bindParam('userid', $username);
        $STH->execute();
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        if ($STH->rowCount() == 1) {
            $saved_password;
            $hash_algorithm;
            while ($row = $STH->fetch()) {
                $saved_password = $row['passwd'];
                $hash_algorithm = $row['encryption_type'];
            }
            echo "hash type: " . $hash_algorithm;
            if ($hash_algorithm != '') {
                $hashed_password = hash($hash_algorithm, $password);
            } else {
                $hashed_password = $password;
            }
            if ($saved_password == $hashed_password) {
                $loginfailed = false;
                $_SESSION['logedInUser'] = true;
                $_SESSION['Username'] = $username;


                // forward to startpage
                if ($_SERVER['SERVER_PROTOCOL'] == 'HTTP/1.1') {
                    if (php_sapi_name() == 'cgi') {
                        header('Status: 303 See Other');
                    } else {
                        header('HTTP/1.1 303 See Other');
                    }
                }

                if (empty($_SERVER['HTTPS']))
                    header('Location: http://' . $hostname . ($path == '/' ? '' : $path) . '/index_user.php');
                else
                    header('Location: https://' . $hostname . ($path == '/' ? '' : $path) . '/index_user.php');
                exit;
            }
        }

    }
    $loginfailed = true;
}
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>

</head>
<body>
<div id="container">
    <h1>Simple DBMail Admin</h1>

    <div id="placeholder-login-panel" class="col-lg-4 col-md-3 col-sm-1 col-xs-0"></div>
    <div class="col-lg-3 col-md-6 col-sm-10 col-xs-12">
        <div class="panel" id="password-panel">
            <div class="panel-heading">
                <h3 class="panel-title">Login</h3>
            </div>
            <div class="panel-body">
                <form id='login' action="login.php" method="post">
                    <input id='username_tx' class='form-control' type="text" name="username" placeholder="Username"/>
                    <input id='password_tx' class='form-control' type="password" name="password"
                           placeholder="Password"/>

                    <div id='login-response'>
                        <?php
                        if ($loginfailed) {
                            echo "Login for $username failed!";
                        }
                        ?>
                    </div>
                    <button type="submit" class="btn btn-default">Login</button>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- Keylistener 'press the enter-key to login if the form is fucused'. -->
<script type="text/javascript">
    function keydown(event) {
        if (!event)
            event = window.event;
        if (event.which) {
            keycode = event.which;
        } else if (event.keyCode) {
            keycode = event.keyCode;
        }
        if (keycode == 13) {
            document.forms['login'].submit();
        }
    }
    document.forms['login'].onkeydown = keydown;
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</html>

