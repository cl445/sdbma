<?php include('auth.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>
    <title>New User</title>
</head>
<body>
<?php include('db_connection.php'); ?>
<?php include('menu.php'); ?>
<div class="container">
    <div id="responseContainer" class="alert hidden" role="alert">
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <span class="sr-only">Error:</span>
        <span id="response"></span>
    </div>
    <h2>New User</h2>

    <form action='' id='new_user' method='post'>
        <table id='user' class="table">

            <tr>
                <th>User ID</th>
                <td><input id="inputUserId" name='userid' type='text' size='30'></td>
                <td><i>The ID/Login, e.g. user@domain.com</i></td>
            </tr>
            <tr>
                <th>Password</th>
                <td><input id="password" name='passwd' type='text' size='30'> Type:
                    <select id='hash_algorithm' name='encryption_type'>
                        <option name='blank'></option>
                        <option name='plaintext'>plaintext</option>
                        <option name='md5-hash'>md5-hash</option>
                        <option name='md5-digest'>md5-digest</option>
                        <option name='whirlpool'>whirlpool</option>
                        <option name='sha512'>sha512</option>
                        <option name='sha256'>sha256</option>
                        <option name='sha1'>sha1</option>
                        <option name='tiger'>tiger</option>
                    </select>
                    <br><a href='javascript:generatePassword()'>generate a Password</a>
                </td>
                <td><i>If you need a plaintext password use the blank option, 'plaintext' will not work.</i></td>
            </tr>
            <tr>
                <th>Hashed Password</th>
                <td><textarea id='hashed_password' name='hashed_password' rows='4' cols='30'></textarea></td>
                <td><i>This will be stored in the database.</i></td>
            </tr>

            <tr>
                <th>Mailbox size</th>
                <td><input id="maxmail_size" name='maxmail_size' type='text' size='10' value='0'> MB</td>
                <td><i>0 means unlimited space.</i></td>
            </tr>

        </table>

            <a class="btn btn-default" href="javascript:saveUser()">Add</a>
            <a class="btn btn-default" href="javascript:document.forms['new_user'].reset()">Reset</a>
    </form>


</div>
</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!-- Keylistener 'Update the hash password value'. -->
<script type="text/javascript">
    function updatePasswordHash(){
        if (($("#hash_algorithm").val() != "") && ($("#hash_algorithm").val() != "plaintext")){
            $.ajax({
                url: "hash_password.php",
                data: { text: $("#password").val(), algorithm:  $("#hash_algorithm").val() },
                context: document.body
            }).done(function(hash) {
                $( "#hashed_password" ).val(hash);
            });
        } else {
            $( "#hashed_password" ).val($("#password").val());
        }
    }
    $( "#password" ).keyup(function() {
        updatePasswordHash();
    });
    $( "#hash_algorithm" ).change(function() {
        updatePasswordHash();
    });

    function generatePassword(){
        $.ajax({
            url: "generate_password.php",
            context: document.body
        }).done(function(hash) {
            $( "#password" ).val(hash);
            updatePasswordHash();
        });

    }

    function saveUser() {
        $.ajax({
            dataType: "json",
            type: "POST",
            url: "api.php",
            data: {
                post: "user",
                userid: $("#inputUserId").val(),
                passwd: $("#hashed_password").val(),
                encryption_type: $("#hash_algorithm").val(),
                maxmail_size: $("#maxmail_size").val()
            },
            context: document.body
        }).done(function (response) {


            if (response.status == 'OK') {
                window.location.href = 'users.php';
            } else {
                $("#responseContainer").removeClass("hidden");
                $("#response").text(response.result);
                $("#responseContainer").removeClass("alert-success");
                $("#responseContainer").addClass("alert-danger");
            }
        });
    }

    jQuery(window).load(function () {
        $("#menu_users").addClass("active");
    });
</script>
<script src="js/bootstrap.min.js"></script>
</html>
 