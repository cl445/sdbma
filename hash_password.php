<?php
/**
 * Hashs a given text with a the given algorithm
 * User: claas
 * Date: 17.11.14
 * Time: 04:48
 */

if ($_GET['algorithm'] == 'sha512')
    $algorithm = 'sha512';
elseif ($_GET['algorithm'] == 'sha256')
    $algorithm = 'sha256';
elseif ($_GET['algorithm'] == 'md5-hash')
    $algorithm = 'md5';
elseif ($_GET['algorithm'] == 'sha1')
    $algorithm = 'sha1';
elseif ($_GET['algorithm'] == 'whirlpool')
    $algorithm = 'whirlpool';
else
    $algorithm = '';

if ($algorithm != '')
    echo hash($algorithm, $_GET['text']);
else
    echo 'Hash algorithm not supported for automated hashing! Paste it here manual.';
?>