<?php include('auth.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>Simple DBMail Admin</title>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>
</head>
<body>

<?php include('menu.php'); ?>


<div class="container">
    <br/>

    <div id="responseContainer" class="alert hidden" role="alert">
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <span class="sr-only">Error:</span>
        <span id="response"></span>
    </div>
    <div id='new_forward_container' class="col-lg-7 col-md-8 col-sm-10 col-xs-12">
        <div id='new_forward' class="form-group row">
            <label for="input_email_address" id='lb_email_adress'
                   class="control-label col-lg-2 col-md-2 col-sm-6 col-xs-12">Email address</label>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12"><input id="input_email_address" type='text' name='alias'
                                                                     class="form-control" placeholder="alias"></div>
            <label for="input_deliver_to" id='lb_deliver_to' class="control-label col-lg-2 col-md-2 col-sm-6 col-xs-12">
                deliver to <span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>
            </label>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <input id="input_deliver_to" type='text' name='deliver_to' class="form-control"
                       placeholder="destination">
            </div>
            <a class="btn btn-default col-lg-2 col-md-2 col-sm-8 col-xs-12" href='JavaScript:saveAlias()'>Add</a>
        </div>
    </div>
</div>

<div class="container">

    <h2>Forwards</h2>


    <div id='list_forwards'>

        <table id='forwards' class='table table-striped table-hover'>
            <thead>
            <tr>
                <th>Aliases</th>
                <th>Deliver to</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
        <div>Number of destination addresses:<span id="numberOfDstAddr"></span></div>

    </div>


    <!-- Keylistener 'press the enter-key to create a new forward, if the form is fucused'. -->
<script type="text/javascript">
    function keydown(event) {
        if (!event)
            event = window.event;
        if (event.which) {
            keycode = event.which;
        } else if (event.keyCode) {
            keycode = event.keyCode;
        }
        if (keycode == 13) {
            saveAlias();
        }
    }
    document.getElementById("new_forward").onkeydown = keydown;
</script>

</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript">
    jQuery(window).load(function () {
        $("#menu_forwards").addClass("active");
        loadForwards();
    });

    //Load list of forwards
    function loadForwards() {
        $("#forwards tbody > tr").remove();
        $.ajax({
            dataType: "json",
            type: "GET",
            url: "api.php",
            data: {
                get: "forwards"
            },
            context: document.body
        }).done(function (response) {
            if (response.status == 'OK') {
                $("#numberOfDstAddr").text(' ' + response.result.length);
                $.each(response.result, function (index, forward) {
                    var row = "<tr>";
                    row += "<td>";
                    $.each(forward.aliases, function (index_aliases, alias) {
                        row += "<a class='forward_del' href='JavaScript: delForward(" + escapeHTML(alias.id) + ")'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></a> ";
                        row += escapeHTML(alias.alias) + "<br>";
                    });
                    row += "<td>" + escapeHTML(forward.deliver_to) + "</td>";
                    row += "</td></tr>";
                    $("#forwards tbody").append(row);
                });
            } else {
                $("#responseContainer").addClass("alert-danger");
                $("#responseContainer").removeClass("hidden");
                $("#response").text(response.result);
            }
        });
    }

    // Save new alias
    function saveAlias() {
        $.ajax({
            dataType: "json",
            type: "POST",
            url: "api.php",
            data: {
                post: "forward",
                alias: $("#input_email_address").val(),
                deliver_to: $("#input_deliver_to").val()
            },
            context: document.body
        }).done(function (response) {
            $("#responseContainer").removeClass("hidden");
            $("#response").text(response.result);

            if (response.status == 'OK') {
                $("#responseContainer").removeClass("alert-danger");
                $("#responseContainer").addClass("alert-success");
            } else {
                $("#responseContainer").removeClass("alert-success");
                $("#responseContainer").addClass("alert-danger");
            }
            loadForwards();
        });
    }

    function delForward(alias_idnr) {
        Check = confirm("Delete that forward? Alias=" + alias_idnr);
        if (Check == true) {
            $.ajax({
                dataType: "json",
                type: "GET",
                url: "api.php",
                data: {
                    delete: "alias",
                    id: alias_idnr
                },
                context: document.body
            }).done(function (response) {
                $("#responseContainer").removeClass("hidden");
                $("#response").text(response.result);

                if (response.status == 'OK') {
                    $("#responseContainer").removeClass("alert-danger");
                    $("#responseContainer").addClass("alert-success");
                } else {
                    $("#responseContainer").removeClass("alert-success");
                    $("#responseContainer").addClass("alert-danger");
                }
                loadForwards();
            });
        }
    }

    function escapeHTML(str) {
        return str.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    }
</script>
<script src="js/bootstrap.min.js"></script>
</html>