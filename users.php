<?php include('auth.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>Simple DBMail Admin</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>
</head>
<body>

<?php include('menu.php'); ?>

<div class="container">

    <div id="responseContainer" class="alert hidden" role="alert">
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <span class="sr-only">Error:</span>
        <span id="response"></span>
    </div>

    <h2>Users</h2>
    <a class="btn btn-default" href='new_user.php'>Add User</a>
    <table id='users' class="table table-striped table-hover">
        <thead>
        <tr>
            <th>User ID</th>
            <th>Mailbox storage</th>
            <th>last login</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <a class="btn btn-default" href='new_user.php'>Add User</a>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript">
    jQuery(window).load(function () {
        $("#menu_users").addClass("active");
    });

    $.ajax({
        dataType: "json",
        type: "GET",
        url: "api.php",
        data: {
            get: "users"
        },
        context: document.body
    }).done(function (response) {
        if (response.status == 'OK') {
            $.each(response.result, function (index, user) {
                var row = '<tr>' +
                    '<td><a href="edit_user.php?user_idnr=' + user.user_idnr + '">' + user.userid + '</a></td>' +
                    '<td>' + user.curmail_size + ' MB / ' + user.maxmail_size + ' MB</td> ';
                if (user.last_login == '1979-11-03 22:05:58') {
                    row += '<td> never </td></tr>';
                } else {
                    row += '<td>' + user.last_login + '</td></tr>';
                }
                $("#users tbody").append(row);
            });
        } else {
            $("#responseContainer").addClass("alert-danger");
            $("#responseContainer").removeClass("hidden");
            $("#response").text(response.result);
        }
    });


</script>
<script src="js/bootstrap.min.js"></script>
</html>
