<?php
/**
 * Created by PhpStorm.
 * User: claas
 * Date: 17.11.14
 * Time: 09:31
 */
class jsonObj
{
    public $result;
    public $status;
}

$json = new jsonObj();

include('auth_user.php');
include('db_connection.php');

$oldPassword = $_POST['oldPassword'];
$newPassword = $_POST['newPassword'];
$newPassword2 = $_POST['newPassword2'];
$username = $_SESSION['Username'];

if (($newPassword == $newPassword2) && ($newPassword != '')) {
    $saved_password;
    $hash_algorithm;
    try {
        $STH = $DBH->prepare('SELECT userid, passwd, encryption_type FROM dbmail_users WHERE userid=:userid');
        $STH->bindParam('userid', $username);
        $STH->execute();
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        while ($row = $STH->fetch()) {
            $saved_password = $row['passwd'];
            $hash_algorithm = $row['encryption_type'];
        }
    } catch (PDOException $e) {
        $json->status = "ERROR";
        $json->result = "Can not do that: " . $e->getMessage();
    }

    if ($hash_algorithm != '') {
        $hashedOldPassword = hash($hash_algorithm, $oldPassword);
    } else {
        $hashedOldPassword = $oldPassword;
    }

    if ($hashedOldPassword == $saved_password) {
        $hashedNewPassword = hash($hash_algorithm, $newPassword);
        try {
            $STH = $DBH->prepare("UPDATE dbmail_users SET passwd=:password WHERE userid=:user");
            $STH->bindParam(':user', $username);
            $STH->bindParam(':password', $hashedNewPassword);

            $STH->execute();

            $json->status = "OK";
            $json->result = "Password successfully updated!";
        } catch (PDOException $e) {
            $json->status = "ERROR";
            $json->result = "Can not do that: " . $e->getMessage();
        }
    } else {
        $json->status = "ERROR";
        $json->result = "The old password is not correct!";
    }
} else {
    $json->status = "ERROR";
    if ($newPassword != $newPassword2) {
        $json->result = "Error: The new passwords are different!";
    } elseif ($newPassword == '') {
        $json->result = "Error: The new password can not be empty!";
    }
}
echo json_encode($json);
?>
