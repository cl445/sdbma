<?php include('auth_user.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>Simple DBMail Admin</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>
</head>
<body>

<?php include('menu_user.php'); ?>


<div class="container">

    <h1>Welcome to <b>Simple DBMail Admin</b></h1>


    <h2>Change Password</h2>

    <div id="responseContainer" class="alert hidden" role="alert">
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <span class="sr-only">Error:</span>
        <span id="response"></span>
    </div>
    <form action="change_password.php" method="post" class="form-horizontal" role="form">
        <div class="form-group">
            <label class="control-label col-sm-2" for="oldPassword">Old password:</label>
            <div class="col-sm-10">
                <input type="password" class="form-control" id="oldPassword" name="oldPassword" placeholder="Old password">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="newPassword">New password:</label>
            <div class="col-sm-10">
                <input type="password" class="form-control" id="newPassword" name="newPassword" placeholder="New password">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="newPassword2">Retype new password:</label>
            <div class="col-sm-10">
                <input type="password" class="form-control" id="newPassword2" placeholder="Retype new password">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <a href="javascript:updatePassword()" class="btn btn-default">Submit</a>
            </div>
        </div>
    </form>

</div>
</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript">
    jQuery(window).load(function () {
        $("#menu_home").addClass("active");
    });

    function updatePassword() {
        $.ajax({
            type: "POST",
            url: "change_password.php",
            data: {
                oldPassword: $("#oldPassword").val(),
                newPassword: $("#newPassword").val(),
                newPassword2: $("#newPassword2").val()
            },
            context: document.body
        }).done(function (response) {
            response = jQuery.parseJSON(response);
            $("#responseContainer").removeClass("hidden");
            $("#response").text(response.result);

            if (response.status == 'OK') {
                $("#responseContainer").removeClass("alert-danger");
                $("#responseContainer").addClass("alert-success");
            } else {
                $("#responseContainer").removeClass("alert-success");
                $("#responseContainer").addClass("alert-danger");
            }

        });

    }
</script>

</html>