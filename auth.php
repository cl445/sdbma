<?php
    session_start();

    $hostname = $_SERVER['HTTP_HOST'];
    $path = dirname($_SERVER['PHP_SELF']);


    if (!isset($_SESSION['logedInAdmin']) || !$_SESSION['logedInAdmin']) {
        if (empty($_SERVER['HTTPS']))
            header('Location: http://'.$hostname.($path == '/' ? '' : $path).'/login.php');
        else
            header('Location: https://'.$hostname.($path == '/' ? '' : $path).'/login.php');
        exit;
     }
?>