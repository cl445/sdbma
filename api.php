<?php
include('auth.php');

// next 2 rows prevent the Browser to cache
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
// The MIME-Type for JSON
header('Content-type: application/json; charset=utf-8');


include('db_connection.php');

class jsonObj
{
    public $result;
    public $status;
}

class user
{
    public $user_idnr;
    public $userid;
    public $encryption_type;
    public $passwd;
    public $curmail_size;
    public $maxmail_size;
    public $last_login;
}


$json = new jsonObj();
if (!(isset($_GET['get']))) {
    $_GET['get'] = '';
}
if (!(isset($_POST['post']))) {
    $_POST['post'] = '';
}
if (!(isset($_GET['delete']))) {
    $_GET['delete'] = '';
}

if ($_GET['get'] == 'users') {
    try {
        $STH = $DBH->prepare("SELECT * FROM dbmail_users WHERE userid NOT IN ('__@!internal_delivery_user!@__', 'anyone', '__public__') ORDER BY userid");
        $STH->execute();
        # setting the fetch mode
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $users = array();
        while ($row = $STH->fetch()) {
            $user = new user();
            $user->user_idnr = $row['user_idnr'];
            $user->userid = $row['userid'];
            $user->curmail_size = round($row['curmail_size'] / 1048576, 2);
            $user->maxmail_size = round($row['maxmail_size'] / 1048576, 2);
            $user->last_login = $row['last_login'];
            $result[] = $user;
        }

        $json->result = $result;
        $json->status = "OK";
    } catch (PDOException $e) {
        $json->result = "DB: " . $e->getMessage();
        $json->status = "ERROR";
    }
} elseif ($_GET['get'] == 'user') {
    if (isset($_GET['user_idnr'])) {
        try {
            $STH = $DBH->prepare('SELECT * FROM dbmail_users WHERE user_idnr=:user_idnr');
            $STH->bindParam(':user_idnr', $_GET['user_idnr']);
            $STH->execute();
            if ($STH->rowCount() == 0) {
                throw new Exception("User_idnr: " + $_GET['user_idnr'] + " is unknown!");
            }
            $STH->setFetchMode(PDO::FETCH_ASSOC);
            $user = new User();
            while ($row = $STH->fetch()) {
                $user->user_idnr = $row['user_idnr'];
                $user->userid = $row['userid'];
                $user->encryption_type = $row['encryption_type'];
                $user->passwd = $row['passwd'];

                // calculating a human readable number for the mailbox size
                $user->curmail_size = round($row['curmail_size'] / 1048576, 2);
                $user->maxmail_size = round($row['maxmail_size'] / 1048576, 2);
                $user->last_login = $row['last_login'];
            }
            $json->result = $user;
            $json->status = "OK";
        } catch (PDOException $e) {
            $json->result = "DB: " . $e->getMessage();
            $json->status = "ERROR";
        } catch (Exception $e) {
            $json->result = $e->getMessage();
            $json->status = "ERROR";
        }
    } else {
        $json->result = "The parameter user_idnr is required!";
        $json->status = "ERROR";

    }
} elseif ($_GET['delete'] == 'user') {
    try {
        if (!(isset($_GET['user_idnr']))) {
            throw new Exception('No user_idnr is set');
        }
        $DBH->beginTransaction();

        $STH1 = $DBH->prepare('DELETE FROM dbmail_aliases WHERE deliver_to=:deliver_to');
        $STH1->bindParam(':deliver_to', $_GET['user_idnr']);
        $STH1->execute();

        $STH2 = $DBH->prepare('DELETE FROM dbmail_users WHERE user_idnr=:deliver_to');
        $STH2->bindParam(':deliver_to', $_GET['user_idnr']);
        $STH2->execute();

        $DBH->commit();

        $json->result = "User with the user_idnr:" . $_GET['user_idnr'] . " deleted!";
        $json->status = "OK";
    } catch (PDOException $e) {
        $json->result = $e->getMessage();
        $json->status = "ERROR";
    } catch (Exception $e) {
        $json->result = $e->getMessage();
        $json->status = "ERROR";
    }
} elseif ($_GET['get'] == 'statistics') {
    class Statistics
    {
        public $number_all_users;
        public $size_all_mailboxes;
        public $number_all_aliases;
        public $number_of_all_messages;
        public $average_messages_day;
    }

    $statistics = new Statistics();

    try {
        $STH = $DBH->prepare('SELECT SUM(curmail_size) AS mboxes_size, COUNT(user_idnr) as nr_users FROM dbmail_users');
        $STH->execute();
        $STH->setFetchMode(PDO::FETCH_ASSOC);

        while ($row = $STH->fetch()) {
            $statistics->number_all_users = ($row['nr_users'] - 3);
            // calculating a human readable number for the mailbox size
            $statistics->size_all_mailboxes = round($row['mboxes_size'] / 1048576, 2);
        }

        $STH = $DBH->prepare('SELECT COUNT(alias_idnr) AS nr_aliases FROM dbmail_aliases');
        $STH->execute();
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        while ($row = $STH->fetch()) {
            $statistics->number_all_aliases = $row['nr_aliases'];
        }

        $STH = $DBH->prepare('select Count(id) as nr_messages from dbmail_physmessage;');
        $STH->execute();
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        while ($row = $STH->fetch()) {
            $statistics->number_of_all_messages = $row['nr_messages'];
        }

        $STH = $DBH->prepare('select Count(id) as nr_messages from dbmail_physmessage where TO_DAYS(internal_date) > TO_DAYS(NOW())-100;');
        $STH->execute();
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        while ($row = $STH->fetch()) {
            $statistics->average_messages_day = ($row['nr_messages'] / 100);
        }

        $json->result = $statistics;
        $json->status = "OK";
    } catch (PDOException $e) {
        $json->result = "DB: " . $e->getMessage();
        $json->status = "ERROR";
    }

} elseif ($_GET['get'] == 'forwards') {
    class Forward
    {
        public $deliver_to;
        public $aliases;
    }

    class Alias
    {
        public $id;
        public $alias;

        public function __construct($id, $alias)
        {
            $this->id = $id;
            $this->alias = $alias;
        }
    }

    try {
        $receivers = array();
        if (isset($_GET['user_idnr'])) {
            if (empty($_GET['user_idnr'])) {
                throw new Exception('The user_idnr is empty!');
            }
            $receivers[] = $_GET['user_idnr'];
        } else {
            $STH = $DBH->query('SELECT DISTINCT deliver_to FROM dbmail_aliases WHERE deliver_to NOT IN (SELECT user_idnr from dbmail_users) ORDER BY deliver_to');
            # setting the fetch mode
            $STH->setFetchMode(PDO::FETCH_ASSOC);
            while ($row = $STH->fetch()) {
                $receivers[] = $row['deliver_to'];
            }
        }
        foreach ($receivers as &$receiver) {
            $forward = new Forward();
            $forward->deliver_to = $receiver;
            $STH2 = $DBH->prepare("SELECT * FROM dbmail_aliases WHERE deliver_to=:deliver_to");
            $STH2->bindParam(":deliver_to", $receiver);
            $STH2->execute();
            # setting the fetch mode
            $STH2->setFetchMode(PDO::FETCH_ASSOC);
            while ($row2 = $STH2->fetch()) {
                $forward->aliases[] = new Alias($row2['alias_idnr'], $row2['alias']);
            }
            $result[] = $forward;
        }

        $json->result = $result;
        $json->status = "OK";
    } catch (PDOException $e) {
        $json->result = "DB: " . $e->getMessage();
        $json->status = "ERROR";
    } catch (Exception $e) {
        $json->result = $e->getMessage();
        $json->status = "ERROR";
    }
} elseif ($_GET['delete'] == "alias") {
    if (isset($_GET['id'])) {
        try {
            $STH = $DBH->prepare("DELETE FROM dbmail_aliases WHERE alias_idnr=:alias_idnr");
            $STH->bindParam(':alias_idnr', $_GET['id']);
            // execute query
            $STH->execute();
            $json->result = "Forward/Alias with the id=" . $_GET['id'] . " deleted!";
            $json->status = "OK";
        } catch (PDOException $e) {
            $json->status = "ERROR";
            $json->result = $e->getMessage();
        }
    } else {
        $json->status = "ERROR";
        $json->result = "The ID parameter is requiered.";
    }

} elseif ($_POST['post'] == 'forward') {
    if ((isset($_POST['alias'])) && (isset($_POST['deliver_to']))) {
        try {
            if (empty($_POST['alias'])) {
                throw new Exception("The alias is empty!");
            }
            if (empty($_POST['deliver_to'])) {
                throw new Exception("The target for the alias is empty!");
            }
            $STH = $DBH->prepare("INSERT INTO dbmail_aliases (alias, deliver_to) VALUES (:alias, :deliver_to)");
            $STH->bindParam(':alias', trim($_POST['alias']));
            $STH->bindParam(':deliver_to', trim($_POST['deliver_to']));

            $STH->execute();
            $json->status = "OK";
            $json->result = "New forward/alias added! " . $_POST['alias'] . " -> " . $_POST['deliver_to'];

        } catch (PDOException $e) {
            $json->status = "ERROR";
            $json->result = "Can not do that: " . $e->getMessage();
        } catch (Exception $e) {
            $json->status = "ERROR";
            $json->result = "Can not do that: " . $e->getMessage();
        }
    } else {
        $json->status = "ERROR";
        $json->result = "Wrong parameter count!";
    }
} elseif ($_POST['post'] == 'user') {

    try {
        // If user_idnr is set update the user otherwise create a new user.
        if ((isset($_POST['user_idnr'])) && (!(empty($_POST['user_idnr'])))) {
            $STH = $DBH->prepare("UPDATE dbmail_users SET userid= :userid, passwd= :passwd, encryption_type= :encryption_type, maxmail_size= :maxmail_size WHERE user_idnr= :user_idnr");
            $STH->bindParam(':user_idnr', $_POST['user_idnr']);
        } else {
            $STH = $DBH->prepare("INSERT INTO dbmail_users (userid, passwd, encryption_type, maxmail_size) VALUES (:userid, :passwd, :encryption_type, :maxmail_size)");
        }
        $STH->bindParam(':userid', trim($_POST['userid']));
        $STH->bindParam(':passwd', trim($_POST['passwd']));
        $STH->bindParam(':encryption_type', $_POST['encryption_type']);
        //calculate the Byte value instead of MB
        $mbox_max = $_POST['maxmail_size'] * 1048576;
        $STH->bindParam(':maxmail_size', $mbox_max);

        $STH->execute();

        $json->status = "OK";
        if ((isset($_POST['user_idnr'])) && (!(empty($_POST['user_idnr'])))) {
            $json->result = "Changes for saved for user ID: " . $_POST['user_idnr'] . "!";
        } else {
            $json->result = "New user created!";
        }

    } catch (PDOException $e) {
        $json->status = "ERROR";
        $json->result = "Can not do that: " . $e->getMessage();
    }
} else {
    $json->result = "Unspecified/Unknown request!";
    $json->status = "ERROR";
}

// Put out Json-Object
// Needed for JSONP callback
if (isset($_GET['callback']))
    echo $_GET['callback'] . "(" . json_encode($json) . ")";
else
    echo json_encode($json);
?>