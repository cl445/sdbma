<?php include('auth.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>Simple DBMail Admin</title>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>
</head>
<body>

<?php include('menu.php'); ?>

<div class="container">

    <div id="responseContainer" class="alert hidden" role="alert">
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <span class="sr-only">Error:</span>
        <span id="response"></span>
    </div>


    <h1>Welcome to <b>Simple DBMail Admin</b></h1>

    <p><b>Version: </b>2.0 Beta - You will find updates <a
            href='#'>here</a>!</p>

    <h2>Statistics</h2>
    <table id='statistics' class='table'>
        <tbody>
        <tr>
            <th>Number of mailusers</th>
            <td id="number_all_users"></td>
        </tr>
        <tr>
            <th>Size of all mailboxes</th>
            <td id="size_all_mailboxes"></td>
        </tr>
        <tr>
            <th>Number of aliases</th>
            <td id="number_all_aliases"></td>
        </tr>
        <tr>
            <th>Number of messages</th>
            <td id="number_of_all_messages"></td>
        </tr>
        <tr>
            <th>Average messages per day <br> (last 100 days)</th>
            <td id="average_messages_day"></td>
        </tr>
        </tbody>
    </table>

</div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript">
    jQuery(window).load(function () {
        $("#menu_home").addClass("active");
    });

    $.ajax({
        dataType: "json",
        type: "GET",
        url: "api.php",
        data: {
            get: "statistics"
        },
        context: document.body
    }).done(function (response) {
        if (response.status == 'OK') {
            var result = response.result;
            $("#number_all_users").text(result.number_all_users);
            $("#size_all_mailboxes").text(result.size_all_mailboxes + " MB");
            $("#number_all_aliases").text(result.number_all_aliases);
            $("#number_of_all_messages").text(result.number_of_all_messages);
            $("#average_messages_day").text(result.average_messages_day);


        } else {
            $("#responseContainer").addClass("alert-danger");
            $("#responseContainer").removeClass("hidden");
            $("#response").text(response.result);
        }
    });

</script>
<script src="js/bootstrap.min.js"></script>
</html>